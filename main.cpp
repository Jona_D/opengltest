#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <cstdio>
#include <cmath>

float trianglePoints[] = {
        0.25, -0.25f, 0,
        0.25, 0.25f, 0,
        -0.25f, -0.25f, 0,
        -0.25f, 0.25f, 0
};

float circlePoints[201 * 3];

const char *vertex_shader =
        "#version 330\n"
        "in vec3 vp;"
        "void main() {"
        "  gl_Position = vec4(vp, 1.0);"
        "}";

const char *fragment_shader =
        "#version 330\n"
        "out vec4 frag_colour;"
        "void main() {"
        "  frag_colour = vec4(0.5, 0.5, 0.5, 0.01);"
        "}";

void compileShaders();

int main() {
    int circleArraySize = sizeof(circlePoints) / sizeof(float);
    for (int i = 0; i < circleArraySize; i += 3) {
        circlePoints[i] = 0.75f * (float) sin((2 * M_PI * i) / (circleArraySize - 3.0));
        circlePoints[i + 1] = 0.75f * (float) cos((2 * M_PI * i) / (circleArraySize - 3.0));
        circlePoints[i + 2] = 0;
        printf("%d, %.3f, %.3f\n", i, circlePoints[i], circlePoints[i + 1]);
    }

    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow *window = glfwCreateWindow(640, 640, "My window", nullptr, nullptr);

    if (!window) {
        std::cout << "Error! Failed to create window.";
        glfwTerminate();
    }

    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK) {
        std::cout << "Error! Failed to initialize GLEW.";
        glfwTerminate();
    }

    // get version info
    const GLubyte *renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte *version = glGetString(GL_VERSION); // version as a string
    printf("Renderer: %s\n", renderer);
    printf("OpenGL version supported %s\n", version);

    compileShaders();

    /*GLuint vbo = 0;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(trianglePoints), trianglePoints, GL_STREAM_DRAW);*/

    GLuint vboCircle = 0;
    glGenBuffers(1, &vboCircle);
    glBindBuffer(GL_ARRAY_BUFFER, vboCircle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(circlePoints), circlePoints, GL_STATIC_DRAW);

    GLuint vao = 0;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    do {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //glNamedBufferSubData(vbo, 0, sizeof(trianglePoints), trianglePoints);

        glBindBuffer(GL_ARRAY_BUFFER, vboCircle);
        glDrawArrays(GL_LINE_STRIP, 0, circleArraySize);

        //glBindBuffer(GL_ARRAY_BUFFER, vbo);
        //glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window));

    glfwTerminate();

    return 0;
}

void compileShaders() {
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertex_shader, nullptr);
    glCompileShader(vs);

    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragment_shader, nullptr);
    glCompileShader(fs);

    GLint success;
    glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLchar log[1024];
        glGetShaderInfoLog(fs, sizeof(log), nullptr, log);
        fprintf(stderr, "Error compiling shader type %d: '%s'\n", GL_FRAGMENT_SHADER, log);
    }

    GLuint shader_programme = glCreateProgram();
    glAttachShader(shader_programme, fs);
    glAttachShader(shader_programme, vs);
    glLinkProgram(shader_programme);

    glUseProgram(shader_programme);

}